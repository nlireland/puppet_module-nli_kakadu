class nli_kakadu {
  package {"unzip":
    ensure => "installed";
  }

  package {"wget":
    ensure => "installed";
  }

  exec {"download-kakadu":
    command => "wget http://kakadusoftware.com/wp-content/uploads/2014/10/KDU74_Demo_Apps_for_Linux-x86-64_140513.zip",
    cwd => "/tmp",
    require =>  package["wget"],
    creates => "/tmp/KDU74_Demo_Apps_for_Linux-x86-64_140513.zip",
    path => ["/usr/local/bin","/usr/bin"],
  }
  exec{"unzip-kakadu":
    command => "unzip /tmp/KDU74_Demo_Apps_for_Linux-x86-64_140513.zip && mv /tmp/KDU74_Demo_Apps_for_Linux-x86-64_140513/* /usr/local/bin/",
    cwd => "/tmp",
    require => [exec["download-kakadu"], package["unzip"]],
    creates => "/usr/local/bin/kdu_compress",
    path => ["/usr/local/bin","/usr/bin"],
  }
  #file_line requires stdlib module
  file_line { 'ld_library_path':
    path => '/etc/environment',
    line => 'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/bin',
    require => exec["unzip-kakadu"],
  }
}
