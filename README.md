# README #

This module installs Kakadu which is a closed source library to encode and decode JPEG 2000 images

### Set up ###

Currently this module is added to the metadata.json file of the puppet_module-nli_hydra repository
```
"dependencies": [
  {"name": "nli/nli_kakadu", "version_requirement": ">=0.0.1" }, ]}
```